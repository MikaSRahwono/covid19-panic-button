from django.urls import path
from .views import login, register, test
from django.urls import path, include

urlpatterns = [
    path('login/', login, name="login"),
    path('register/', register, name="register"),
    path('test/', test, name="test"),
]
