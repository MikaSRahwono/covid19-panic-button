from django.shortcuts import render
from django.contrib.auth import authenticate, login as auth_login
from django.http import JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt

# credits: https://gist.github.com/Meta502/1605fdba3b141fbf67dba689e9e55498

@csrf_exempt
def login(request):
    username = request.POST['username']
    password = request.POST['password']
    print(username, " ", password)
    user = authenticate(username=username, password=password)
    if user is not None:
        auth_login(request, user)
        # Redirect to a success page.
        return JsonResponse({
          "status": True,
          "message": "login success"
        }, status=200)

    else:
        return JsonResponse({
          "status": False,
          "message": "login failed"
        }, status=401)


@csrf_exempt
def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        username = request.POST['username']
        password = request.POST['password1']
        password_confirmation = request.POST['password2']

        if password != password_confirmation:
            return JsonResponse({
              "status": False,
              "message": "register failed: password confirmation mismatch"
            }, status=401)
        elif len(password) < 8:
            return JsonResponse({
              "status": False,
              "message": "register failed: password too short"
            }, status=401)
        elif form.is_valid():
            user = form.save()
            auth_login(request, user)
            return JsonResponse({
              "status": True,
              "message": "register success"
            }, status=200)

        return JsonResponse({
            "status": False,
            "message": "register failed: other (weak password or username taken)"
        }, status=401)



@csrf_exempt
def test(request):
    return JsonResponse({"test": "test"}, status=200)
