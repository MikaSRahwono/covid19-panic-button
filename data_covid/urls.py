from django.urls import path
from .views import index, index_json

urlpatterns = [
    path('', index, name='index'),
    path('json', index_json, name='index_json'),
]
