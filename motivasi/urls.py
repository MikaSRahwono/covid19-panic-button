from django.urls import path
from .views import index, index_json, post_flutter

urlpatterns = [
    path('', index, name='index'),
    path('index_json', index_json, name='index_json'),
    path('post_flutter', post_flutter, name='post_flutter')
]