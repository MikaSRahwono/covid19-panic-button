import json
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from motivasi.forms import MotivasiForm
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from motivasi.models import Motivasi

# Create your views here.
@login_required(login_url="/login/")
def index(request):
    motivasi = Motivasi.objects.all()
    response = {'motivasi' : motivasi}
    # print(request.POST)
    form = MotivasiForm()
    if request.method == 'POST' :
        form = MotivasiForm(request.POST)
        # checking
        print(form)
        if form.is_valid():
            form.save()
    response['form'] = form
    return render(request, 'motivasi.html', response)

def index_json(request):
    # print(request.body)
    # test = json.loads(request.body)
    # print(test)
    # motivasi = Motivasi.objects.all()
    
    # data = serializers.serialize('json', motivasi)
    # test = motivasi['model']
    # print(test)
    # print(data)
    # return HttpResponse(data, content_type="application/json")

    print(Motivasi.objects.all().values())
    data = list(Motivasi.objects.all().values())
    test = data[0]
    print(test['message'])
    return JsonResponse(data, safe=False)

@csrf_exempt
def post_flutter(request):
    print(request.method)
    print(request.body)
    data = json.loads(request.body)
    print(data['message'])
    newData = Motivasi(message = data['message'])
    newData.save()
    return JsonResponse({'Status' : 'Berhasil'})