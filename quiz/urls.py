from django.urls import path
from .views import index
from .views import hasil
from .views import mobile

urlpatterns = [
    path('', index, name='index'),
    path('hasil', hasil, name = 'hasil'),
    path('mobile', mobile, name = 'mobileQuiz')
]
