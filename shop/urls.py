from django.urls import path
from .views import *

urlpatterns = [
    path('', BarangView.as_view(), name='shop'),
    path('ambil_konten', ambil_konten, name="ambil_konten"),
    path('post_konten', post_konten, name='post_konten')
]