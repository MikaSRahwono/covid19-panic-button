from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView
from .models import Barang, Feedback
from .forms import BarangForm
from django.views.decorators.csrf import csrf_exempt
import json
from django.http.response import HttpResponse, JsonResponse


@csrf_exempt
def ambil_konten(request):
    tampung = []
    obj = Barang.objects.all()
    for i in obj:
        tampung.append({
            "pilihan": i.pilihan,
            "gambar": i.gambar,
            "link": i.link
        })
    tampung = json.dumps(tampung)
    return HttpResponse(tampung, content_type='application/json')

@csrf_exempt
def post_konten(request):
    data = json.loads(request.body)
    newData = Feedback(tanggapan = data['tanggapan'])
    newData.save()
    return JsonResponse({'tanggapan' : 'ok'})

class BarangView(TemplateView):
    template_name = 'shop.html'

    def get(self, request):
        form = BarangForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        barang = {}
        form = BarangForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['the_choices']
            form = BarangForm()
            barang = Barang.objects.filter(pilihan=text)
        args = {'form': form, 'text': text, 'barang': barang}
        return render(request, self.template_name, args)
